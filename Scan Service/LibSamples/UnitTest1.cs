﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;

namespace LibSamples
{
    [TestClass]
    public class UnitTest1
    {
        private string inDir = @".\Images";
        private string outFile = "out.pdf";

        [TestMethod]
        public void TestMethod1()
        {
            var document = new Document();
            var section = document.AddSection();
            
            foreach (var file in Directory.EnumerateFiles(inDir))
            {
                var img = section.AddImage(file);

                img.RelativeHorizontal = RelativeHorizontal.Page;
                img.RelativeVertical = RelativeVertical.Page;

                img.Top = 0;
                img.Left = 0;

                img.Height = document.DefaultPageSetup.PageHeight;
                img.Width = document.DefaultPageSetup.PageWidth;

                section.AddPageBreak(); // разделитель после каждой картинки
            }

            var render = new PdfDocumentRenderer();
            render.Document = document;

            render.RenderDocument();
            render.Save(outFile);
        }
    }
}
