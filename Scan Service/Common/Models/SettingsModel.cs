﻿using System.Xml.Serialization;

namespace Common.Models
{
    [XmlType("Settings")]
    public class SettingsModel
    {
        [XmlElement("PageInterval")]
        public string PageInterval { get; set; }

        public override bool Equals(object obj)
        {
            SettingsModel settings = obj as SettingsModel;
            if (settings != null)
            {
                return settings.PageInterval == PageInterval;
            }

            return false;
        }
    }
}
