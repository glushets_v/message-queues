﻿namespace Common.Models
{
    public enum InputServiceStatus
    {
        Wait,
        Process
    }
}