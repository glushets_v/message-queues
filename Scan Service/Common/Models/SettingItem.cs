﻿namespace Common.Models
{
    public class SettingItem
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}