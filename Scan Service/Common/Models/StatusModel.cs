﻿using System.Collections.Generic;

namespace Common.Models
{
    public class StatusModel
    {
        public string Name { get; set; }

        public InputServiceStatus Status { get; set; }

        public List<SettingItem> Settings { get; set; }
    }
}
