﻿using System.IO;

namespace Common.Helpers
{
    public class FileHelper
    {
        public static byte[] ReadFile(string fileLocation)
        {
            return File.ReadAllBytes(fileLocation);
        }

        public static void SaveFile(string filePath, byte[] bytes)
        {
            File.WriteAllBytes(filePath, bytes);
        }
    }
}
