﻿namespace Common
{
    public interface IWorker
    {
        void Start();
        void Stop();
    }
}
