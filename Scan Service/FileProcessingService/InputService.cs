﻿using System.Collections.Generic;
using Common;
using Common.Models;
using FileProcessingService.Workers;

namespace FileProcessingService
{
    public class InputService
    {
        private string _fileWorkerQueue;
        private string _statusWorkerQueue;
        private string _settingsWorkerQueue;

        private List<IWorker> _workers;

        public static InputServiceStatus ServiceStatus { get; set; }

        public InputService()
        {
            _fileWorkerQueue = @".\Private$\FileWorkerQueue";
            _statusWorkerQueue = @".\Private$\StatusWorkerQueue";
            _settingsWorkerQueue = @".\Private$\SettingsWorkerQueue";

            _workers = new List<IWorker>();
        }

        public void Start()
        {
            StartWorker(new InputFileWorker(_fileWorkerQueue));
            StartWorker(new InputStatusWorker(_statusWorkerQueue));
            StartWorker(new InputSettingsWorker(_settingsWorkerQueue));
        }

        public void Stop()
        {
            foreach (var worker in _workers)
            {
                worker.Stop();
            }
        }

        private void StartWorker(IWorker worker)
        {
            worker.Start();
            _workers.Add(worker);
        }
    }
}
