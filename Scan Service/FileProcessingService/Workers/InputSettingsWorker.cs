﻿using System.Configuration;
using System.Messaging;
using System.Threading;
using Common;
using Common.Models;

namespace FileProcessingService.Workers
{
    public class InputSettingsWorker : IWorker
    {
        private Thread _workingThread;
        private ManualResetEvent _workStop;

        private MessageQueue _queue;

        public InputSettingsWorker(string queueName)
        {
            _workingThread = new Thread(Work);
            _workStop = new ManualResetEvent(false);

            InitQueue(queueName);
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public void Stop()
        {
            _workStop.Set();
            _workingThread.Join();
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }
        }

        private void Work()
        {
            do
            {
                if (_queue.CanRead)
                {
                    using (_queue)
                    {
                        var message = _queue.Receive();

                        if (message != null)
                        {
                            ProcessData((SettingsModel)message.Body);
                        }
                    }
                }
            } while (true);
        }

        private void ProcessData(SettingsModel messageBody)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            config.AppSettings.Settings["checkInterval"].Value = messageBody.PageInterval;

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
