﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Common;
using Common.Models;

namespace FileProcessingService.Workers
{
    public class InputFileWorker : IWorker
    {
        private FileSystemWatcher _watcher;

        private string _inDir;
        private string _outDir;

        private Thread _workingThread;
        private ManualResetEvent _workStop;
        private AutoResetEvent _newFile;

        private PdfCreator _documentCreator;
        private readonly string[] _validExtensions = { "jpg", "bmp", "gif", "png", "jpeg" };

        private int _folderNum = 1;
        private string _outFileName = "out.pdf";

        private DateTime _lastFileDate;

        private const int TimeOut = 3;

        public InputFileWorker(string queueName)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            _inDir = Path.Combine(currentDir, "in");
            _outDir = Path.Combine(currentDir, "out");

            if (!Directory.Exists(_inDir))
            {
                Directory.CreateDirectory(_inDir);
            }

            if (!Directory.Exists(_outDir))
            {
                Directory.CreateDirectory(_outDir);
            }

            _workingThread = new Thread(WorkProc);
            _workStop = new ManualResetEvent(false);
            _newFile = new AutoResetEvent(false);

            _watcher = new FileSystemWatcher(_inDir);
            _watcher.Created += FileCreate;

            _documentCreator = new PdfCreator(queueName);

            _lastFileDate = DateTime.Now;
        }

        public void Start()
        {
            _workingThread.Start();
            _watcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            _watcher.EnableRaisingEvents = false;
            _workStop.Set();
            _workingThread.Join();
        }

        private void WorkProc()
        {
            string moveDirName = string.Concat(_outDir, _folderNum);
            var imageNumBefore = 0;

            InputService.ServiceStatus = InputServiceStatus.Wait;

            do
            {
                InputService.ServiceStatus = InputServiceStatus.Process;

                var files = Directory.EnumerateFiles(_inDir).ToArray();

                for (int i = 0; i < files.Count(); i++)
                {
                    var fileName = Path.GetFileName(files[i]);
                    var splitedFileName = SplitFileName(fileName);

                    if (CheckImageFormate(splitedFileName.Last().ToLower()))
                    {
                        int num;
                        if (int.TryParse(splitedFileName[1], out num))
                        {
                            if (num != imageNumBefore + 1 || !CheckTimout())
                            {
                                _documentCreator.CreatePdf(moveDirName, string.Concat(_folderNum, "_", _outFileName));
                                _folderNum++;
                                moveDirName = string.Concat(_outDir, _folderNum);
                            }

                            if (!MoveFile(files[i], fileName, moveDirName)) // если еще не дали команду на остановку
                            {
                                return;
                            }

                            if (i == files.Count() - 1)
                            {
                                _documentCreator.CreatePdf(moveDirName, string.Concat(_folderNum, "_", _outFileName));
                            }

                            imageNumBefore = num;
                            _lastFileDate = DateTime.Now;
                        }
                    }
                }
            } while (WaitHandle.WaitAny(new WaitHandle[] { _workStop, _newFile }) != 0);

            InputService.ServiceStatus = InputServiceStatus.Wait;
        }

        private bool CheckTimout()
        {
            TimeSpan diff = DateTime.Now.Subtract(_lastFileDate);

            return diff.TotalMinutes >= 0 && diff.TotalMinutes <= TimeOut;
        }

        private bool MoveFile(string file, string fileName, string outDirName)
        {
            if (_workStop.WaitOne(TimeSpan.Zero)) // если еще не дали команду на остановку
            {
                return false;
            }

            if (TryOpen(file, 3))
            {
                if (!Directory.Exists(outDirName))
                {
                    Directory.CreateDirectory(outDirName);
                }

                File.Move(file, Path.Combine(outDirName, fileName));
            }

            return true;
        }

        private bool CheckImageFormate(string fileExt)
        {
            return _validExtensions.Contains(fileExt);
        }

        private string[] SplitFileName(string fileName)
        {
            return fileName.Split('.', '_');
        }

        private void FileCreate(object sender, FileSystemEventArgs e)
        {
            _newFile.Set(); // инит события что новый файл появился
        }

        private bool TryOpen(string fullPath, int v)
        {
            for (int j = 0; j < v; j++)
            {
                try
                {
                    var file = File.Open(fullPath, FileMode.Open, FileAccess.Read, FileShare.None); // открываем в монопольном доступе
                    file.Close();

                    return true;
                }
                catch (IOException)
                {
                    Thread.Sleep(3000);
                }
            }
            return false;
        }
    }
}
