﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Models;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace FileProcessingService.Workers
{
    public class InputStatusWorker : IWorker
    {
        private Thread _workingThread;
        private ManualResetEvent _workStop;
        private int _statusTimeInterval;

        private MessageQueue _queue;

        public InputStatusWorker(string queueName)
        {
            _statusTimeInterval = int.Parse(ConfigurationManager.AppSettings["statusTimeInterval"]);

            _workingThread = new Thread(Work);
            _workStop = new ManualResetEvent(false);

            InitQueue(queueName);
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public void Stop()
        {
            _workStop.Set();
            _workingThread.Join();
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }
        }

        private void Work()
        {
            while (true)
            {
                var statusModel = new StatusModel
                {
                    Name = Environment.MachineName,
                    Status = InputService.ServiceStatus,
                    Settings = new List<SettingItem>
                    {
                        new SettingItem { Name = "checkInterval", Value = ConfigurationManager.AppSettings["statusTimeInterval"] }
                    }
                };

                _queue.Send(statusModel);
                
                Task.Delay(_statusTimeInterval).Wait();
            }
        }
    }
}
