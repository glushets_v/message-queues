﻿using System.IO;
using System.Messaging;
using Common.Helpers;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;

namespace FileProcessingService
{
    public class PdfCreator
    {
        private MessageQueue _queue;

        public PdfCreator(string queueName)
        {
            InitQueue(queueName);
        }

        public void CreatePdf(string inDir, string outFileName)
        {
            var document = new Document();
            var section = document.AddSection();

            foreach (var file in Directory.EnumerateFiles(inDir))
            {
                var img = section.AddImage(file);

                img.RelativeHorizontal = RelativeHorizontal.Page;
                img.RelativeVertical = RelativeVertical.Page;

                img.Top = 0;
                img.Left = 0;

                img.Height = document.DefaultPageSetup.PageHeight;
                img.Width = document.DefaultPageSetup.PageWidth;

                section.AddPageBreak(); // разделитель после каждой картинки
            }

            var render = new PdfDocumentRenderer
            {
                Document = document
            };

            render.RenderDocument();

            render.Save(Path.Combine(inDir, outFileName));

            SendToQueue(Path.Combine(inDir, outFileName));
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }
        }

        private void SendToQueue(string file)
        {
            byte[] byteArray = FileHelper.ReadFile(file);

            _queue.Send(byteArray);
        }
    }
}
