﻿using System.Diagnostics;
using System.IO;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Common;
using Common.Models;

namespace CentralServer.Workers
{
    public class ServerSettingsWorker : IWorker
    {
        private const int CheckINtarval = 20000;

        private MessageQueue _queue;
        private Thread _workingThread;
        private ManualResetEvent _workStop;

        private string _pathToSettingsFile;
        private SettingsModel _settingsModel;

        public ServerSettingsWorker(string queueName)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var setDir = Path.Combine(currentDir, "settings");
            _pathToSettingsFile = Path.Combine(setDir, "settings.xml");

            if (!Directory.Exists(setDir))
            {
                Directory.CreateDirectory(setDir);
            }

            if (!File.Exists(_pathToSettingsFile))
            {
                _settingsModel = new SettingsModel();
                _settingsModel.PageInterval = "5000";
                SaveSettingsFile(_pathToSettingsFile, _settingsModel);
            }
            else
            {
                _settingsModel = LoadSettingsFile(_pathToSettingsFile);
            }

            _workStop = new ManualResetEvent(false);

            InitQueue(queueName);
            _workingThread = new Thread(CheckSendSettings);
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public void Stop()
        {
            _workStop.Set();
            _workingThread.Join();
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }
        }

        private void CheckSendSettings()
        {
            while (true)
            {
                SettingsModel newSettings;
                if (!File.Exists(_pathToSettingsFile))
                {
                    newSettings = new SettingsModel();
                    newSettings.PageInterval = "5000";
                    SaveSettingsFile(_pathToSettingsFile, _settingsModel);
                }
                else
                {
                    newSettings = LoadSettingsFile(_pathToSettingsFile);
                }

                if (!newSettings.Equals(_settingsModel))
                {
                    _queue.Send(newSettings);
                    _settingsModel = newSettings;
                    SaveSettingsFile(_pathToSettingsFile, _settingsModel);
                }

                Task.Delay(CheckINtarval).Wait();
            }
        }

        private void SaveSettingsFile(string path, SettingsModel settings)
        {
            using (var writer = new XmlTextWriter(path, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;
                XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
                serializer.Serialize(writer, settings);
            }
        }

        private SettingsModel LoadSettingsFile(string path)
        {
            using (var reader = new XmlTextReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
                return (SettingsModel)serializer.Deserialize(reader);
            }
        }
    }
}
