﻿using System;
using System.Diagnostics;
using System.IO;
using System.Messaging;
using System.Threading;
using Common;
using Common.Helpers;

namespace CentralServer.Workers
{
    public class ServerFileWorker : IWorker
    {
        private MessageQueue _queue;
        private Thread _workingThread;
        private ManualResetEvent _workStop;
        private string _outDir;
        private int _fileNumber = 1;


        public ServerFileWorker(string queueName)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            _outDir = Path.Combine(currentDir, "out");

            if (!Directory.Exists(_outDir))
            {
                Directory.CreateDirectory(_outDir);
            }

            _workStop = new ManualResetEvent(false);

            InitQueue(queueName);
            _workingThread = new Thread(ReceiveData);
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public void Stop()
        {
            _workStop.Set();
            _workingThread.Join();
        }

        private void ReceiveData()
        {
            do
            {
                if (_queue.CanRead)
                {
                    using (_queue)
                    {
                        var message = _queue.Receive();

                        if (message != null)
                        {
                            ProcessData((byte[])message.Body);
                        }
                    }
                }
            } while (true);
        }

        private void ProcessData(byte[] data)
        {
            var filePath = Path.Combine(_outDir, string.Concat(_fileNumber, "_file.pdf"));

            FileHelper.SaveFile(filePath, data);

            _fileNumber++;

            Console.WriteLine(filePath);
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }

            _queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(byte[]) });
        }
    }
}
