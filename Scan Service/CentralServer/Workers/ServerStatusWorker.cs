﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using CentralServer.Models;
using Common;
using Common.Models;

namespace CentralServer.Workers
{
    public class ServerStatusWorker : IWorker
    {
        private MessageQueue _queue;
        private Thread _workingThread;
        private ManualResetEvent _workStop;

        private string _outDir;
        private string _pathToStatusFile;

        private StatusesModel _statusesModel;

        public ServerStatusWorker(string queueName)
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            _outDir = Path.Combine(currentDir, "outStatus");
            _pathToStatusFile = Path.Combine(_outDir, "inputStatus.xml");

            if (!Directory.Exists(_outDir))
            {
                Directory.CreateDirectory(_outDir);
            }

            if (!File.Exists(_pathToStatusFile))
            {
                _statusesModel = new StatusesModel();
                SaveStatusFile(_pathToStatusFile, _statusesModel);
            }
            else
            {
                _statusesModel = LoadStatusFile(_pathToStatusFile);
            }


            _workStop = new ManualResetEvent(false);

            InitQueue(queueName);
            _workingThread = new Thread(Work);
        }

        public void Start()
        {
            _workingThread.Start();
        }

        public void Stop()
        {
            _workStop.Set();
            _workingThread.Join();
        }

        private void SaveStatusFile(string path, StatusesModel statuses)
        {
            using (var writer = new XmlTextWriter(path, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;
                XmlSerializer serializer = new XmlSerializer(typeof(StatusesModel));
                serializer.Serialize(writer, statuses);
            }
        }

        private StatusesModel LoadStatusFile(string path)
        {
            using (var reader = new XmlTextReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(StatusesModel));
                return (StatusesModel)serializer.Deserialize(reader);
            }
        }

        private void InitQueue(string queueName)
        {
            if (MessageQueue.Exists(queueName))
            {
                _queue = new MessageQueue(queueName);
            }
            else
            {
                _queue = MessageQueue.Create(queueName);
            }

            _queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(StatusModel) });
        }

        private void Work()
        {
            do
            {
                if (_queue.CanRead)
                {
                    using (_queue)
                    {
                        var message = _queue.Receive();

                        if (message != null)
                        {
                            ProcessData((StatusModel)message.Body);
                        }
                    }
                }
            } while (true);
        }

        private void ProcessData(StatusModel statusModel)
        {
            var savedStatusModel = _statusesModel.Statuses.FirstOrDefault(x => x.Name == statusModel.Name);
            if (savedStatusModel != null)
            {
                savedStatusModel.Status = statusModel.Status;
                savedStatusModel.Settings = statusModel.Settings;
            }
            else
            {
                _statusesModel.Statuses.Add(statusModel);
            }

            SaveStatusFile(_pathToStatusFile, _statusesModel);
        }
    }
}
