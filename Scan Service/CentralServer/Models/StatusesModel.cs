﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Common.Models;

namespace CentralServer.Models
{
    [XmlType("Statuses")]
    public class StatusesModel
    {
        [XmlArray("Statuses"), XmlArrayItem(typeof(StatusModel), ElementName = "Status")]
        public List<StatusModel> Statuses { get; set; }
    }
}
