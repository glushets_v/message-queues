﻿using System.Collections.Generic;
using CentralServer.Workers;
using Common;

namespace CentralServer
{
    public class CentralServer
    {
        private List<IWorker> _workers;

        private string _fileWorkerQueue;
        private string _statusWorkerQueue;
        private string _settingsWorkerQueue;

        public CentralServer()
        {
            _workers = new List<IWorker>();

            _fileWorkerQueue = @".\Private$\FileWorkerQueue";
            _statusWorkerQueue = @".\Private$\StatusWorkerQueue";
            _settingsWorkerQueue = @".\Private$\SettingsWorkerQueue";
        }

        public void Start()
        {
            StartWorker(new ServerFileWorker(_fileWorkerQueue));
            StartWorker(new ServerStatusWorker(_statusWorkerQueue));
            StartWorker(new ServerSettingsWorker(_settingsWorkerQueue));
        }

        public void Stop()
        {
            foreach (var worker in _workers)
            {
                worker.Stop();
            }
        }

        private void StartWorker(IWorker worker)
        {
            worker.Start();
            _workers.Add(worker);
        }
    }
}
